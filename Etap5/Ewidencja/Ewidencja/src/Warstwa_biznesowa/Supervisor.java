/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Warstwa_biznesowa;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.zip.DataFormatException;

/**
 *
 * @author Martella
 */
public class Supervisor {
    PersonBuilder personBuilder = new PersonBuilder();
    
    public Person getPerson() 
    {
    
        return personBuilder.getPerson();
    }
    
    public void setBuilder(PersonBuilder builder) 
    {
    
        this.personBuilder=builder;
    }
    
    public void constructPerson(List<String> data) throws DataFormatException
    {
        
        personBuilder.makePerson();
        personBuilder.addPesel(data.get(0));
        
        Sex sex;
        if(Sex.KOBIETA.toString().equals(data.get(1)))
            sex=Sex.KOBIETA;
        else if(Sex.MEZCZYZNA.toString().equals(data.get(1)))
            sex=Sex.MEZCZYZNA;
        else throw new DataFormatException("Błędny format płci");
        personBuilder.addSex(sex);
        
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(data.get(2), formatter);
        personBuilder.addBirthdate(date);
        
        personBuilder.addFullName(data.subList(3, 5));
        
        MaritalStatus status;
        if(MaritalStatus.WOLNY.toString().equals(data.get(5)))
            status=MaritalStatus.WOLNY;
        else if (MaritalStatus.ZAMEZNY_ZONATA.toString().equals(data.get(5)))
            status=MaritalStatus.ZAMEZNY_ZONATA;
        else throw new DataFormatException("Błędny format stanu cywillnego");
        personBuilder.addMaritalStatus(status);
        
        personBuilder.addFullAddress(data.subList(6, 9));
        personBuilder.addAnother(data.subList(9,data.size()));
        
    }
    
}
