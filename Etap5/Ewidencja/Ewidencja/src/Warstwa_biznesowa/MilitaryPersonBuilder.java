/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Warstwa_biznesowa;

import java.util.List;
import java.util.zip.DataFormatException;

/**
 *
 * @author Martella
 */
public class MilitaryPersonBuilder extends PersonBuilder {
   @Override
   public void makePerson() 
   {
       person = new MilitaryPerson();
   }
   @Override
   public void addAnother(List<String> another) throws DataFormatException
   {
       if(another.size()!=2) throw new DataFormatException("Błędny format danych");
       person.setNationality(another.get(0));
       MilitaryStatus status;
       if(MilitaryStatus.A.toString().equals(another.get(1)))
           status=MilitaryStatus.A;
       else if(MilitaryStatus.B.toString().equals(another.get(1)))
           status=MilitaryStatus.B;
       else if(MilitaryStatus.C.toString().equals(another.get(1)))
           status=MilitaryStatus.C;
       else if(MilitaryStatus.D.toString().equals(another.get(1)))
           status=MilitaryStatus.D;
       else if(MilitaryStatus.E.toString().equals(another.get(1)))
           status=MilitaryStatus.E;
       else throw new DataFormatException("Błędny format kategorii wojskowej");
       ((MilitaryPerson)person).setMilitaryStatus(status);       
   }
}
