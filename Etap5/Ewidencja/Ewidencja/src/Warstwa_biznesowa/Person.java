/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Warstwa_biznesowa;

import java.time.LocalDate;
import java.util.Objects;


/**
 *
 * @author Martella
 */


public class Person {
   protected String pesel;
   protected String name;
   protected String surname;
   protected Sex sex;
   protected LocalDate birthdate;
   protected String street;
   protected String addressNumber;
   protected String apartment;
   protected String nationality;
   protected MaritalStatus maritalStatus;
    
    public void setPesel(String val) {this.pesel = val;}
    public void setName(String val) {this.name = val;}
    public void setSurname(String val) {this.surname = val;}
    public void setSex (Sex val) {this.sex = val;}
    public void setBirthdate(LocalDate val) {this.birthdate = val;}
    public void setStreet(String val) {this.street = val;}
    public void setAddressNumber (String val) {this.addressNumber = val;}
    public void setApartment(String val) {this.apartment = val;}
    public void setNationality (String val) {this.nationality = val;}
    public void setMaritalStatus(MaritalStatus val) {this.maritalStatus = val;}
    
    public String getPesel(){ return pesel;}
    public String getName() { return name;}
    public String getSurname() { return surname;}
    public Sex getSex () {return sex;}
    public LocalDate getBirthdate() {return birthdate;}
    public String getStreet() {return street;}
    public String getAddressNumber () {return addressNumber;}
    public String getApartment() {return apartment;}
    public String getNationality () {return nationality;}
    public MaritalStatus getMaritalStatus() {return maritalStatus;}

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.pesel);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (!Objects.equals(this.pesel, other.pesel)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Person{" + "pesel=" + pesel + ", name=" + name + ", surname=" + surname + ", sex=" + sex + ", birthdate=" + birthdate + ", street=" + street + ", addressNumber=" + addressNumber + ", apartment=" + apartment + ", nationality=" + nationality + ", maritalStatus=" + maritalStatus + "}\n";
    }
    
    
}
