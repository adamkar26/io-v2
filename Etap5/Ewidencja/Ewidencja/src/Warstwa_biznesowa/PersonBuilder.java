/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Warstwa_biznesowa;

import java.time.LocalDate;
import java.util.List;
import java.util.zip.DataFormatException;

/**
 *
 * @author Martella
 */
public class PersonBuilder {
    
    Person person;
    
    public void makePerson() 
    {
        person = new Person();
    }
    
    public Person getPerson()
    {
        return person;
    }
    
    public void addPesel(String pesel) throws DataFormatException
    {
        if(pesel.length()!=11) throw new DataFormatException("Błędny format PESEL");
        int control =0;
        int[] weight= {9,7,3,1,9,7,3,1,9,7};
        for(int i =0; i<10;i++)
        {
            control += weight[i]*Integer.parseInt(pesel.substring(i, i+1));
        }
        control %= 10;
        if(Integer.parseInt(pesel.substring(10))!=control)
            throw new DataFormatException("Błędny format PESEL");
        
        person.setPesel(pesel);
        
        
    }
    
    public void addSex(Sex sex) throws DataFormatException
    {
        int sexNumber = Integer.parseInt(person.getPesel().substring(9,10))%2;
        if(sexNumber==1 && sex==Sex.KOBIETA) 
            throw new DataFormatException("Błędna płeć");
        if(sexNumber==0 && sex==Sex.MEZCZYZNA) 
            throw new DataFormatException("Błędna płeć");
        person.setSex(sex);
        
    }
    
    public void addBirthdate(LocalDate date) throws DataFormatException
    {
        String year = Integer.toString(date.getYear());
        int shortYear = Integer.parseInt(year.substring(2));
        
        int century = Integer.parseInt(year.substring(0,2));
        
        int month = date.getMonthValue();
        
        int day = date.getDayOfMonth();
        
        
        if(Integer.parseInt(person.getPesel().substring(0,1))==shortYear) 
            throw new DataFormatException("Błędna data");
        
        if(century==19)
        {
            if(Integer.parseInt(person.getPesel().substring(2,4))!=month)
                throw new DataFormatException("Błędna data");
        }
        else if(century==20)
        {
            if(Integer.parseInt(person.getPesel().substring(2,4))!=month+20)
                throw new DataFormatException("Błędna data");
        }
        
        if(day!=Integer.parseInt(person.getPesel().substring(4,6)))
            throw new DataFormatException("Błędna data");
        
        person.setBirthdate(date);
    }
    
    public void addFullName(List <String> fullName) throws DataFormatException
    {
    
        if(fullName.size()!=2) throw new DataFormatException("Błędne imie/nazwisko");
        person.setName(fullName.get(0));
        person.setSurname(fullName.get(1));
       
    }
    
    public void addMaritalStatus(MaritalStatus status) 
    {
        person.setMaritalStatus(status);
    }
    
    public void addFullAddress(List<String> fullAddress) throws DataFormatException
    {
        if(fullAddress.size()!=3) throw new DataFormatException("Błędny adres");
        person.setStreet(fullAddress.get(0));
        person.setAddressNumber(fullAddress.get(1));
        if(fullAddress.size()==3) 
            person.setApartment(fullAddress.get(2));
    }
    
    public void addAnother(List <String> another) throws DataFormatException
    {
        if(another.size()!=1) throw new DataFormatException("Błędne dane");
        person.setNationality(another.get(0));
    }
}
