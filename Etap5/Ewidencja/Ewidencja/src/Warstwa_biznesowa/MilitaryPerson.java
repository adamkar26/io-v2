/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Warstwa_biznesowa;

/**
 *
 * @author Martella
 */


public class MilitaryPerson extends Person {
    private MilitaryStatus militaryStatus;
    public MilitaryPerson() {};
    public MilitaryStatus getMilitaryStatus() {return militaryStatus;}
    public void setMilitaryStatus(MilitaryStatus status) {militaryStatus=status;}

    @Override
    public String toString() {
       return "MilitaryPerson{" +super.toString()+ "militaryStatus=" + militaryStatus + "}\n";
    }
    
}
